//
//  utils.cpp
//  tetris-clone
//
//  Fonctions utiles
//
//  Created by Arthur Pourrier on 20/04/2018.
//  Copyright © 2018 artnou. All rights reserved.
//

#include "utils.h"

//Renvoie un nombre aléatoire entre 0 et (nb - 1)
//Ne génère pas le même nombre 2 fois de suite
int getRandomNumber(int nb) {
    int num;
    static int last = 0;
    while (true) {
        num = rand() % nb;
        if (num != last) {
            last = num;
            break;
        }
    }
    return num;
}
