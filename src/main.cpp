//
//  render.cpp
//  tetris-clone
//
//  Fichier principal - contient la fonction main()
//
//  Created by Arthur Pourrier on 20/04/2018.
//  Copyright © 2018 artnou. All rights reserved.
//

//Utiliser SDL et iostream
#include <time.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include "render.h"             //les fonctions de rendu de la grille et des tetriminos
#include "debug.h"              //fonctions pour le débogage
#include "audio.h"              //Bibliothèque SDL pour simplifier la gestion du son et de la musique

//En-têtes de fonctions
SDL_bool initializeSDL();
SDL_bool initializeBlocks();
SDL_bool initializeEverything();
void startNewGame();
void putOnHold();
Tetrimino popNext();
Tetrimino rotate(Tetrimino tetrimino, DIRECTION direction);
Tetrimino move(Tetrimino tetrimino, DIRECTION direction);
SDL_bool isPossible(Tetrimino tetrimino);
SDL_bool isCompleteLine(Uint8 line[16]);
int removeCompleteLines();
void setShadowFromCurrent();
void setCurrentTetrimino(Tetrimino tetrimino);
void mergeCurrentWithGrid();
void handleTurn();
int main(int argc, char* args[]);

//Fenetre de l'application
SDL_Window* window = NULL;

//Renderer pour le rendu graphique
SDL_Renderer* renderer;

//Les textures utilisées dans le jeu
SDL_Texture* blocksTexture[7] = { 0 };
SDL_Texture* backgroundImage = NULL;
SDL_Texture* logo = NULL;
SDL_Texture* score = NULL;
SDL_Texture* lines = NULL;
SDL_Texture* level = NULL;

//Dimensions et Coordonnées des différentes zones
SDL_Rect gridRect = { 240, 20, GRID_WIDTH, GRID_HEIGTH };
SDL_Rect holdRect = { MARGIN, 80, SIDE_LENGTH, SIDE_LENGTH };
SDL_Rect scoreRect = { MARGIN, 600, SIDE_LENGTH, 40 };
SDL_Rect linesRect = { MARGIN, 360, SIDE_LENGTH, 40 };
SDL_Rect levelRect = { MARGIN, 480, SIDE_LENGTH, 40 };
SDL_Rect nextRect[3] = {
    { 680, 80, SIDE_LENGTH, SIDE_LENGTH },
    { 680, 320, SIDE_LENGTH, SIDE_LENGTH },
    { 680, 560, SIDE_LENGTH, SIDE_LENGTH }};

//Matrice de la grille
Uint8 gridMatrix[23][16] = { 0 };

//Tetriminos
Tetrimino currentTetrimino;
Tetrimino holdTetrimino;
Tetrimino nextTetrimino[3];
Tetrimino shadowTetrimino;

//Gerer le temps qui s'ecoule entre chaque tour
Uint32 currentTick = SDL_GetTicks();
Uint32 lastTick = currentTick; //Let's go
Uint32 delayTick = DELAY_START;

//Compteurs du jeu
Uint32 scoreCounter = 0;
Uint32 linesCounter = 0;
Uint32 levelCounter = 0;
int holdUsed = 0; //1 si on a utilisé la réserve

//Démarrage du jeu
GAME_STATE state = startScreen;
MUSIC music = on;

//Sons
Audio* audioTetris;
Audio* audioLine;
Audio* audioTheme;

//Fonts
TTF_Font* smallFont;
TTF_Font* bigFont;
TTF_Font* bigGameFont;
TTF_Font* gameFont;
SDL_Color textColor = { 255, 255, 255, 255 }; // white
SDL_Color menuTextColor = { 0, 0, 0, 210 }; // white

//Initialise SDL, crée la fenêtre de l'application et le "renderer" pour dessiner dessus
SDL_bool initializeSDL() {
    //Initialisation de SDL
    if( SDL_Init( SDL_INIT_EVENTS | SDL_INIT_VIDEO | SDL_INIT_AUDIO ) < 0 )
    {
        SDL_Log("Could not initialize SDL: %s", SDL_GetError());
        return SDL_FALSE;
    }
    
    //Creer la fenetre de l'application
    Uint32 flags = SDL_WINDOW_ALLOW_HIGHDPI;
    window = SDL_CreateWindow( "Tetris Clone", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                SCREEN_WIDTH, SCREEN_HEIGHT, flags );
    if( window == NULL )
    {
        SDL_Log("Could not create window: %s", SDL_GetError());
        return SDL_FALSE;
    }

	//Créer le renderer
    renderer = SDL_CreateRenderer( window, -1, 0 );
	if ( renderer == NULL )
	{
        SDL_Log("Failed to create renderer: %s", SDL_GetError());
        return SDL_FALSE;
	}
    
    //Permettre le rendu de la transparence
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

	//Le renderer fait la taille de la fenêtre
	SDL_RenderSetLogicalSize(renderer, SCREEN_WIDTH, SCREEN_HEIGHT);

    return SDL_TRUE;
}

//Crée un tableau de Textures à partir de la sprite sheet
//Chaque élément du tableau représente un bloc de couleur
SDL_bool initializeBlocks() {
    
    //Charger la sprite sheet (l'image contenant les blocks de couleur)
    SDL_Surface* sprites = IMG_Load("images/tetris-blocks.png");
    if ( sprites == NULL )
    {
        SDL_Log("Could not load image: %s", SDL_GetError());
        return SDL_FALSE;
    }

    //Récupérer les coordonnées des blocs de couleurs de la sprite sheet
    //Chaque bloc fait SQUARE_LENGTH de côté, les couleurs ne sont pas dans le bon ordre
    SDL_Rect blocks[7] = {
        { 0 * SQUARE_LENGTH, 0, SQUARE_LENGTH, SQUARE_LENGTH },
        { 3 * SQUARE_LENGTH, 0, SQUARE_LENGTH, SQUARE_LENGTH },
        { 2 * SQUARE_LENGTH, 0, SQUARE_LENGTH, SQUARE_LENGTH },
        { 1 * SQUARE_LENGTH, 0, SQUARE_LENGTH, SQUARE_LENGTH },
        { 4 * SQUARE_LENGTH, 0, SQUARE_LENGTH, SQUARE_LENGTH },
        { 5 * SQUARE_LENGTH, 0, SQUARE_LENGTH, SQUARE_LENGTH },
        { 6 * SQUARE_LENGTH, 0, SQUARE_LENGTH, SQUARE_LENGTH }
    };
    
    //Récupérer chaque bloc de couleur sous la forme d'une texture
    for(int i = 0; i < 7; i++) {
        SDL_Texture* block = getBlockTexture(sprites, blocks[i]);
        blocksTexture[i] = block;
    }
    
    //Libérer la mémoire
    SDL_FreeSurface(sprites);
    
    return SDL_TRUE;
}

SDL_bool initializeTTF()
{
	//Initialiser SDL2_TTF
	if ( TTF_Init() < 0 )
	{
        SDL_Log("Failed to initialize TTF: %s", SDL_GetError());
        return SDL_FALSE;
	}

	//Charger les fontes
	smallFont = TTF_OpenFont("font/Xefus.ttf", 32 );
    bigFont = TTF_OpenFont("font/Xefus.ttf", 128 );
    bigGameFont = TTF_OpenFont("font/Games.ttf", 48 );
    gameFont = TTF_OpenFont("font/Games.ttf", 32 );

	// Error check
    if ( smallFont == NULL || bigFont == NULL || bigGameFont == NULL || gameFont == NULL )
	{
        SDL_Log("Failed to load font: %s", SDL_GetError());
        return SDL_FALSE;
	}

	return SDL_TRUE;
}

void initializeAudio() {
    //Initialiser la bibliothèque Simple-SDL2-Audio
    initAudio();

    //Charger les fichiers audio en mémoire
    audioTetris = createAudio("audio/60141__uzerx__guitar-noise-2.wav", 0, SDL_MIX_MAXVOLUME / 2);
    audioLine = createAudio("audio/188040__antumdeluge__child-s-xylophone-1.wav", 0, SDL_MIX_MAXVOLUME / 2);
    audioTheme = createAudio("audio/tetris.wav", 0, SDL_MIX_MAXVOLUME / 2);
}

//Initialise l'application, charge le fond et la couleur de dessin
SDL_bool initializeEverything() {
    //Initialiser le generateur de nombre aléatoire
    srand((Uint8)time(NULL));
    
    if (!initializeSDL()) {
        SDL_Log("Failed to init SDL");
        return SDL_FALSE;
    }

    if (!initializeBlocks()) {
        SDL_Log("Failed to init Blocks");
        return SDL_FALSE;
    }

    if (!initializeTTF()) {
        SDL_Log("Failed to init TTF");
        return SDL_FALSE;
    }

    initializeAudio();
    
    //Définir la couleur du renderer à gris foncé
	SDL_SetRenderDrawColor( renderer, 60, 60, 70, 255 );
    
    //Charger le fond
    backgroundImage = loadTexture("images/tetris-bg-metal.jpg");
    
    //Charger le logo
    logo = loadTexture("images/tetris-logo.png");

    return SDL_TRUE;
}

//Remets tout à zéro pour démarrer une nouvelle partie
//
void startNewGame() {
    //Remettre les compteurs à 0
    scoreCounter = 0;
    linesCounter = 0;
    levelCounter = 0;
    currentTick = SDL_GetTicks();
    lastTick = currentTick; //Let's go
    delayTick = DELAY_START;

    //Vider la grille
    memset(gridMatrix, 0, sizeof(gridMatrix));

    //Vider les Tetriminos
    setCurrentTetrimino(EmptyTetrimino);
    holdTetrimino = EmptyTetrimino;

    //Tirer les 3 prochains tetriminos au hasard
    for (int indice = 0; indice < 3; indice++) {
        int rnd = getRandomNumber(7);
        nextTetrimino[indice] = EmptyTetrimino;
        nextTetrimino[indice].model = rnd + 1;
        nextTetrimino[indice].rotation = 0;
        memcpy(nextTetrimino[indice].matrix, TETRIMINOS[rnd][0], sizeof(nextTetrimino[indice].matrix));
    }
}

//Calculer le délai entre 2 tours en fonction du niveau
//Plus le niveau est élevé, plus le délai est réduit et plus ça va vite
void setDelayTick() {
    Uint32 delay = DELAY_START - levelCounter * DELAY_OFFSET;
    delayTick = (delay < DELAY_MIN) ? DELAY_MIN : delay;
}

//Echanger le tetrimino en cours avec la réserve si on ne l'a pas déjà fait
void putOnHold() {
    if (holdTetrimino.model > 0) {
        Tetrimino tmp = holdTetrimino;
        tmp.position = START_POSITION; // Toujours faire apparaître en haut
        holdTetrimino = currentTetrimino;
        setCurrentTetrimino(tmp);
        tmp = EmptyTetrimino;
    } else {
        //Rien dans la réserve
        holdTetrimino = currentTetrimino;
        setCurrentTetrimino(EmptyTetrimino);
    }
    holdUsed = 1;
}

//Récuperer le prochain tetrimino, tout décaler et tirer un 3e au hasard
Tetrimino popNext() {
    //Récupérer le premier tetrimino
    Tetrimino popped = nextTetrimino[0];
    
    //Position de départ dans la grille
    popped.position = START_POSITION;

    //Décaler les prochains tétriminos
    nextTetrimino[0] = nextTetrimino[1];
    nextTetrimino[1] = nextTetrimino[2];

    //Tirer le 3e Tetrimino au hasard
    int rnd = getRandomNumber(7);
    nextTetrimino[2] = EmptyTetrimino;
    nextTetrimino[2].model = rnd + 1;
    nextTetrimino[2].rotation = 0;
    memcpy(nextTetrimino[2].matrix, TETRIMINOS[rnd][0], sizeof(nextTetrimino[2].matrix));
    
    return popped;
}

//Rotation du tetrimino
Tetrimino rotate(Tetrimino tetrimino, DIRECTION direction) {
    //4 rotations possible
    if (direction == right) {
        tetrimino.rotation = (tetrimino.rotation + 1) % 4;
    } else {
        tetrimino.rotation = (tetrimino.rotation + 3) % 4;
    }
    //Récupérer la matrice correspondante
    memcpy(tetrimino.matrix, TETRIMINOS[tetrimino.model - 1][tetrimino.rotation], sizeof(tetrimino.matrix));

    return tetrimino;
}

//Déplacement du tetrimino
Tetrimino move(Tetrimino tetrimino, DIRECTION direction) {
    if (direction == left) {
        tetrimino.position.x -= 1;
    } else if (direction == right) {
        tetrimino.position.x += 1;
    } else if (direction == down) {
        tetrimino.position.y -= 1;
    }
    return tetrimino;
}

//EST POSSIBLE
SDL_bool isPossible(Tetrimino tetrimino) {
    //Peut-on placer ce tetrimino dans la grille ?
    SDL_bool possible = SDL_TRUE;
    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 4; col++) {
            if (tetrimino.matrix[row][col] != 0) {
                if ((tetrimino.position.x + col < 3) //On sort de la grille à gauche
                    || (tetrimino.position.x + col > 12) //On sort de la grille à droite
                    || (tetrimino.position.y + row < 1) //On ne peut pas aller plus bas
                    || (gridMatrix[tetrimino.position.y + row][tetrimino.position.x + col] != 0)) //la case est occupée
                {
                    possible = SDL_FALSE;
                    break; // premier for
                }
            }
        }
        if (!possible) { break; }//inutile d'aller plus loin
    }
    return possible;
}

//Retourne vrai si la ligne est complète
SDL_bool isCompleteLine(Uint8 line[16]) {
    SDL_bool isComplete = SDL_TRUE;
    for(Uint8 col=0; col<16; col++ ) {
        if (col > 2 && col < 13) {
            if (line[col] == 0) { isComplete = SDL_FALSE; }
        }
    }
    return isComplete;
}

//Retire les lignes complètes de la grille et en renvoie le nombre
int removeCompleteLines() {

    Uint8 newGridMatrix[23][16] = { 0 };
    int newIndice = 0;
    
    int nbLines = 0;
    for(Uint8 row=0; row<23; row++ ) {
        if (isCompleteLine(gridMatrix[row])) {
            nbLines += 1 ;
        } else {
            memcpy(newGridMatrix[newIndice], gridMatrix[row], sizeof(newGridMatrix[0]));
            newIndice +=1;
        }
    }

    //Remplacer la grille par la nouvelle
    memcpy(gridMatrix, newGridMatrix, sizeof(gridMatrix));
    
    return nbLines;
}

//Calculer l'ombre du tetrimino en cours
void setShadowFromCurrent() {
    if (currentTetrimino.model == 0 ) {
        shadowTetrimino = EmptyTetrimino;
    } else {
        SDL_bool canGoDown = SDL_TRUE;
        shadowTetrimino = currentTetrimino;
        while (canGoDown) {
            Tetrimino tetrimino = move(shadowTetrimino, down);
            if (isPossible(tetrimino)) {
                shadowTetrimino = tetrimino;
            } else {
                canGoDown = SDL_FALSE;
            }
        }
        
        if (shadowTetrimino.position.y == currentTetrimino.position.y) {
            //Même position que currentTetrimino
            shadowTetrimino = EmptyTetrimino;
        }
    }
}

//Assigner la valeur au tetrimino en cours et calculer l'ombre
void setCurrentTetrimino(Tetrimino tetrimino) {
    currentTetrimino = tetrimino;
    setShadowFromCurrent();
}

//Fusionne la matrice du Tetrimino avec la grille
void mergeCurrentWithGrid() {
    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 4; col++) {
            if (currentTetrimino.matrix[row][col] != 0) {
                gridMatrix[currentTetrimino.position.y + row][currentTetrimino.position.x + col] = currentTetrimino.matrix[row][col];
            }
        }
    }
}

//Calcul du score (voir http://tetris.wikia.com/wiki/Scoring)
void calculerScore(Uint32 nbLines) {
    switch (nbLines) {
        case 4: scoreCounter += 1200 * (levelCounter + 1); break; 
        case 3: scoreCounter += 300 * (levelCounter + 1); break; 
        case 2: scoreCounter += 100 * (levelCounter + 1); break; 
        default: scoreCounter += 40 * (levelCounter + 1);
    }
}

//TRAITER LE TOUR
void handleTurn() {
    
    int nbLines = removeCompleteLines();
    if (nbLines > 0) {
        calculerScore(nbLines);
        if (nbLines == 4) {
            //Tetris
            playSoundFromMemory(audioTetris, SDL_MIX_MAXVOLUME);
        } else {
            playSoundFromMemory(audioLine, SDL_MIX_MAXVOLUME);
        }
        linesCounter += nbLines;
        levelCounter = linesCounter / 10;
        setDelayTick();
    }
    
    if (currentTetrimino.model > 0) {
        //Il y a un tetrimino en cours
        Tetrimino tetrimino = move(currentTetrimino, down);
        if (isPossible(tetrimino)) {
            setCurrentTetrimino(tetrimino);
        } else {
            mergeCurrentWithGrid();
            setCurrentTetrimino(EmptyTetrimino);
        }
    } 
    
    if (currentTetrimino.model == 0) {
        //Pas de tetrimino en cours, on récupère le prochain
        Tetrimino tetrimino = popNext();
        if (isPossible(tetrimino)) {
            setCurrentTetrimino(tetrimino);
            // On peut réutiliser la réserve
            holdUsed = 0;
        } else {
            //On ne peut pas placer le nouveau Tetrimino
            state = gameOver;
        }
    }
}

//DEMARRAGE DE L'APPLICATION
int main( int argc, char* args[] )
{
    if (!initializeEverything()) {
        return EXIT_FAILURE;
    }

    //Variable pour traquer les evenements
    SDL_Event windowEvent;
    
    //On ne s'arrete que quand l'utilisateur ferme la fenetre (quit) ou tape sur Q
    SDL_bool loop = SDL_TRUE;
    while ( loop )
    {
        //Gerer les evenements (touche pressee, fermeture fenetre...)
        if ( SDL_PollEvent( &windowEvent ) )
        {
            if ( SDL_QUIT == windowEvent.type )
            {
                loop = SDL_FALSE;
                break;
            }
            
            if ( SDL_KEYDOWN == windowEvent.type )
            {
                //Touche enfoncee
                switch (windowEvent.key.keysym.sym)
                {
                    case SDLK_LEFT:
                        if (state == playing) {
                            //Déplacer le tetrimino en cours vers la gauche si possible
                            Tetrimino tetrimino = move(currentTetrimino, left);
                            if (isPossible(tetrimino)) {
                                setCurrentTetrimino(tetrimino);
                            }
                        }
                        break;
                    case SDLK_RIGHT:
                        if (state == playing) {
                            //Déplacer le tetrimino en cours vers la droite si possible
                            Tetrimino tetrimino = move(currentTetrimino, right);
                            if (isPossible(tetrimino)) {
                                setCurrentTetrimino(tetrimino);
                            }
                        }
                        break;
                    case SDLK_ESCAPE:
                        //Quitter
                        loop = SDL_FALSE;
                        break;
                    case SDLK_w:
                        if (state == playing) {
                            //Rotation du tetrimino en cours vers la gauche si possible
                            Tetrimino tetrimino = rotate(currentTetrimino, left);
                            if (isPossible(tetrimino)) {
                                setCurrentTetrimino(tetrimino);
                            }
                        }
                        break;
                    case SDLK_x:
                        if (state == playing) {
                            //Rotation du tetrimino en cours vers la droite si possible
                            Tetrimino tetrimino = rotate(currentTetrimino, right);
                            if (isPossible(tetrimino)) {
                                setCurrentTetrimino(tetrimino);
                            }
                        }
                        break;
                    case SDLK_UP:
                        if (state == playing) {
                            //Descendre le tetrimino tout en bas
                            //Score: chaque case "descendue" donne +2
                            SDL_bool canGoDown = SDL_TRUE;
                            int bonus = 0;
                            while (canGoDown) {
                                Tetrimino tetrimino = move(currentTetrimino, down);
                                if (isPossible(tetrimino)) {
                                    setCurrentTetrimino(tetrimino);
                                    bonus +=2;
                                } else {
                                    canGoDown = SDL_FALSE;
                                    scoreCounter += bonus;
                                }
                            }
                            shadowTetrimino = EmptyTetrimino;
                        }
                        break;
                    case SDLK_DOWN:
                        if (state == playing) {
                            //Descendre le tetrimino vers le bas
                            //Score: +1
                            Tetrimino tetrimino = move(currentTetrimino, down);
                            if (isPossible(tetrimino)) {
                                setCurrentTetrimino(tetrimino);
                                scoreCounter += 1;
                            }
                        }
                        break;
                    case SDLK_SPACE:
                        if (state == playing) {
                            //Mise en réserve si possible
                            Tetrimino tetrimino = holdTetrimino;
                            tetrimino.position = START_POSITION;
                            if (!holdUsed && isPossible(tetrimino)) {
                                putOnHold();
                            }
                        }
                        break;
                    case SDLK_m:
                        //musique on/off
                        music = ( music == on) ? off : on;
                        if (music == off) {
                            pauseAudio(); //Simple-SDL2-Audio
                        } else {
                            unpauseAudio(); //Simple-SDL2-Audio
                        }
                        break;
                    case SDLK_p:
                        if (state == playing) {
                            //mettre sur pause
                            state = paused;
                        }
                        break;
                    case SDLK_RETURN:
                        if (state == startScreen || state == gameOver) {
                            //Tout remettre à 0
                            startNewGame();
                            playMusicFromMemory(audioTheme, SDL_MIX_MAXVOLUME);
                            if (music == on) {
                                unpauseAudio();
                            }

                            //Début du jeu
                            state = playing;

                        } else if (state == paused) {
                            //reprendre le jeu
                            state = playing;
                        }
                        break;
                }
            }
        }
        
        //Vérifier si le temps est ecoule
        //Si c'est le cas et qu'on n'est pas en pause, on traite le tour
        currentTick = SDL_GetTicks();
        if (state == playing && (currentTick - lastTick >= delayTick)) {
            handleTurn();
            
            if (state == gameOver) {
                //Afficher le score et l'écran de Game Over
                if (music == on) { pauseAudio(); }

            } else {
                //Remise a zero de l'ecart
                lastTick = currentTick;
            }
        }
        
        render(state);
    }
    
    //Libérer la bibliothèque Simple-SDL2-Audio
    pauseAudio();
    endAudio();
    audioTetris = NULL;
    audioLine = NULL;
    audioTheme = NULL;

    //Libérer les fonts
    TTF_CloseFont(smallFont);
    TTF_CloseFont(bigFont);
    TTF_CloseFont(bigGameFont);
    TTF_CloseFont(gameFont);
    TTF_Quit();

    //Libérer la mémoire
    SDL_DestroyTexture(backgroundImage);
    SDL_DestroyTexture(logo);
    for (int i = 0; i < 7; i++) {
        SDL_DestroyTexture(blocksTexture[i]);
    }
    SDL_DestroyTexture(score);
    SDL_DestroyTexture(lines);
    SDL_DestroyTexture(level);

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    //Quitter les sous-systemes SDL
    SDL_Quit();
    
    return EXIT_SUCCESS;
}
