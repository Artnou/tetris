//
//  utils.h
//  tetris-clone
//
//  Fonctions utiles
//
//  Created by Arthur Pourrier on 20/04/2018.
//  Copyright © 2018 artnou. All rights reserved.
//

#ifndef utils_h
#define utils_h

#include <stdlib.h>

int getRandomNumber(int nb);

#endif /* utils_h */
