//
//  render.h
//  tetris-clone
//
//  Fonctions liées à l'affichage et aux textures
//
//  Created by Arthur Pourrier on 20/04/2018.
//  Copyright © 2018 artnou. All rights reserved.
//

#ifndef render_h
#define render_h

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include "utils.h"
#include "tetris_constants.h" //Les structures et constantes de l'application (taille écran, marge...)

void render(GAME_STATE state);
void renderGrid();
void renderTetrimino(Tetrimino tetrimino, SDL_Rect location);
void renderStartScreen();
void renderGameScreen();
void renderNext();
void renderHold();
void renderCurrentTetrimino();
void renderCounter(SDL_Rect counterRect, Uint32 counterValue);
void renderCounters();

SDL_Texture* loadTexture( const std::string &str );
SDL_Texture* getBlockTexture(SDL_Surface* spriteSheet, SDL_Rect clip);
SDL_Texture* surfaceToTexture(SDL_Surface* surface);

extern SDL_Window* window;
extern SDL_Renderer* renderer;

extern SDL_Texture* blocksTexture[7];
extern SDL_Texture* backgroundImage;
extern SDL_Texture* logo;
extern SDL_Texture* score;
extern SDL_Texture* lines;
extern SDL_Texture* level;

//Coordonnées des différentes zones
extern SDL_Rect gridRect;
extern SDL_Rect holdRect;
extern SDL_Rect scoreRect;
extern SDL_Rect linesRect;
extern SDL_Rect levelRect;
extern SDL_Rect nextRect[3];

//Matrices
extern Uint8 gridMatrix[23][16];

//Tetriminos
extern Tetrimino currentTetrimino;
extern Tetrimino shadowTetrimino;
extern Tetrimino holdTetrimino;
extern Tetrimino nextTetrimino[3];

//Compteurs du jeu
extern Uint32 scoreCounter;
extern Uint32 linesCounter;
extern Uint32 levelCounter;

//Fonts et couleurs du texte
extern TTF_Font* smallFont;
extern TTF_Font* bigFont;
extern TTF_Font* bigGameFont;
extern TTF_Font* gameFont;

extern SDL_Color textColor;
extern SDL_Color menuTextColor;

#endif /* render_h */





