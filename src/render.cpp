//
//  render.cpp
//  tetris-clone
//
//  Fonctions liées à l'affichage et aux textures
//
//  Created by Arthur Pourrier on 20/04/2018.
//  Copyright © 2018 artnou. All rights reserved.
//

#include "render.h"

//Afficher le fond d'écran
void renderBackground() {
    SDL_RenderCopy( renderer, backgroundImage, NULL, NULL );
}

//Afficher la grille
void renderGrid() {
    //Effacer la grille
    SDL_RenderFillRect(renderer, &gridRect);

    //Dessiner la grille
    for(Uint8 row=0; row<23; row++ ) {
        for(Uint8 col=0; col<16; col++ ) {
            if (col > 2 && col < 13 && row > 0 && row < 21) {
                Uint8 value = gridMatrix[row][col];
                if (value > 0) {
                    // Calcule X et Y en fonction de ligne, colonne et SQUARE_LENGTH (blockRect)
                    SDL_Rect blockRect = {
                        gridRect.x + (col - 3) * SQUARE_LENGTH,
                        gridRect.y + GRID_HEIGTH - row * SQUARE_LENGTH,
                        SQUARE_LENGTH,
                        SQUARE_LENGTH
                    };
                    SDL_RenderCopy(renderer, blocksTexture[value - 1], NULL, &blockRect);
                }
            }
        }
    }
}

//Afficher un tetrimino
 void renderTetrimino(Tetrimino tetrimino, SDL_Rect location, SDL_bool shadow) {

    //Couleur de fond légèrement transparente
    SDL_SetRenderDrawColor( renderer, 60, 60, 70, 220 );

    for(Uint8 row=0; row<4; row++ ) {
        for(Uint8 col=0; col<4; col++ ) {
            Uint8 value = tetrimino.matrix[row][col];
                if (value > 0) {
                    // Calcule X et Y en fonction de ligne, colonne et SQUARE_LENGTH (blockRect)
                    SDL_Rect blockRect = {
                        location.x + col * SQUARE_LENGTH,
                        location.y + (3 - row) * SQUARE_LENGTH,
                        SQUARE_LENGTH,
                        SQUARE_LENGTH
                    };

                    //Ne pas afficher la partie éventuellement au dessus de la grille
                    if (blockRect.y >= gridRect.y ) {
                        SDL_RenderCopy(renderer, blocksTexture[value - 1], NULL, &blockRect);
                        if (shadow) {
                            //Si c'est l'ombre, on dessine un carré transparent au dessus
                            SDL_RenderFillRect(renderer, &blockRect);
                        }
                    }
                }
        }
    }

    //Restaurer la couleur par défaut
    SDL_SetRenderDrawColor( renderer, 60, 60, 70, 255 );

}

//Afficher le tetrimino en cours
void renderCurrentTetrimino() {
    if (currentTetrimino.model > 0) {
        //Calcul des coordonnées du Tetrimino à l'écran
        SDL_Rect currentRect = {
            gridRect.x + (currentTetrimino.position.x - 3) * SQUARE_LENGTH,
            gridRect.y + GRID_HEIGTH - (currentTetrimino.position.y + 3) * SQUARE_LENGTH
        };
        renderTetrimino(currentTetrimino, currentRect, SDL_FALSE);
    }
}

//Afficher l'ombre du tetrimino en cours
void renderShadowTetrimino() {
    if (shadowTetrimino.model > 0) {
        //Calcul des coordonnées de l'ombre à l'écran
        SDL_Rect currentRect = {
            gridRect.x + (shadowTetrimino.position.x - 3) * SQUARE_LENGTH,
            gridRect.y + GRID_HEIGTH - (shadowTetrimino.position.y + 3) * SQUARE_LENGTH
        };
        renderTetrimino(shadowTetrimino, currentRect, SDL_TRUE);
    }
}

//Afficher les prochains tetriminos
void renderNext() {
    for(int i = 0; i < 3; i++) {
        SDL_RenderFillRect(renderer, &nextRect[i]);
        renderTetrimino(nextTetrimino[i], nextRect[i], SDL_FALSE);
    }
}

//Afficher la réserve
void renderHold() {
    //Effacer la zone de réserve
    SDL_RenderFillRect(renderer, &holdRect);

    //Faire le rendu du tetrimino en réserve
    if (holdTetrimino.model > 0) {
        renderTetrimino(holdTetrimino, holdRect, SDL_FALSE);
    }
}

//Afficher la valeur d'un compteur à l'écran (justifié à droite)
void renderCounter(TTF_Font *font, SDL_Rect counterRect, Uint32 counterValue) {
    //La valeur est un Uint32, on le convertit en char
    char str[8];
    sprintf(str, "%d", counterValue);
    
    //Récupérer une texture qui contient le texte
    SDL_Surface* textSurface = TTF_RenderText_Blended(font, str, textColor );
	SDL_Texture* textTexture = surfaceToTexture(textSurface);

    //Récupérer les dimensions du texte
    SDL_Rect textRect;
	SDL_QueryTexture(textTexture, NULL, NULL, &textRect.w, &textRect.h );

    //Calculer la position pour que le texte soit toujours justifié à droite
    textRect.x = counterRect.x + (counterRect.w - textRect.w) - 4; // Aligné à droite (marge de 4 pixels)
    textRect.y = counterRect.y + (counterRect.h - textRect.h);
    
    //Dessiner le fond
    SDL_RenderFillRect(renderer, &counterRect);

    //Faire le rendu du texte
    SDL_RenderCopy(renderer, textTexture, NULL, &textRect);

    //Libérer la mémoire
    SDL_DestroyTexture(textTexture);
}

//Afficher les compteurs
void renderCounters() {
    renderCounter(smallFont, scoreRect, scoreCounter);
    renderCounter(smallFont, linesRect, linesCounter);
    renderCounter(smallFont, levelRect, levelCounter);
}

void renderTextCentered(TTF_Font *font, const char *text, SDL_Rect destinationRect) {
    //Récupérer une texture qui contient le texte
    SDL_Surface* textSurface = TTF_RenderText_Blended(font, text, menuTextColor );
    SDL_Texture* textTexture = surfaceToTexture(textSurface);

    //Récupérer les dimensions du texte
    SDL_Rect textRect;
    SDL_QueryTexture(textTexture, NULL, NULL, &textRect.w, &textRect.h );
    
    //Calculer la position pour que le texte soit toujours centré
    textRect.x = destinationRect.x + (destinationRect.w - textRect.w) / 2;
    textRect.y = destinationRect.y + (destinationRect.h - textRect.h) / 2;
   
    //Faire le rendu du texte
    SDL_RenderCopy(renderer, textTexture, NULL, &textRect);
    
    //Libérer la mémoire
    SDL_DestroyTexture(textTexture);
}

void renderText(TTF_Font *font, const char *text, SDL_Rect destinationRect) {
    //Récupérer une texture qui contient le texte
    SDL_Surface* textSurface = TTF_RenderText_Blended(font, text, menuTextColor );
    SDL_Texture* textTexture = surfaceToTexture(textSurface);
    
    //Récupérer les dimensions du texte
    SDL_Rect textRect;
    SDL_QueryTexture(textTexture, NULL, NULL, &textRect.w, &textRect.h );
    
    //Calculer la position du texte
    textRect.x = destinationRect.x;
    textRect.y = destinationRect.y - 40;
    
    //Faire le rendu du texte
    SDL_RenderCopy(renderer, textTexture, NULL, &textRect);
    
    //Libérer la mémoire
    SDL_DestroyTexture(textTexture);
}

void renderBig(TTF_Font *font, const char *text) {
    //Récupérer une texture qui contient le texte
    SDL_Surface* textSurface = TTF_RenderText_Blended(font, text, textColor );
    SDL_Texture* textTexture = surfaceToTexture(textSurface);
    
    //Récupérer les dimensions du texte
    SDL_Rect textRect;
    SDL_QueryTexture(textTexture, NULL, NULL, &textRect.w, &textRect.h );
    
    //Calculer la position pour que le texte soit toujours justifié à droite
    textRect.x = (SCREEN_WIDTH - textRect.w) / 2;
    textRect.y = (SCREEN_HEIGHT - textRect.h) / 2;
    
    //Faire le rendu du texte
    SDL_RenderCopy(renderer, textTexture, NULL, &textRect);
    
    //Libérer la mémoire
    SDL_DestroyTexture(textTexture);
}

//Afficher l'écran de démarrage
void renderStartScreen() {
    SDL_RenderClear( renderer );
    renderBackground();

    //Récupérer les dimensions du logo
    SDL_Rect logoRect;
    SDL_QueryTexture(logo, NULL, NULL, &logoRect.w, &logoRect.h );
    logoRect.x = (SCREEN_WIDTH - logoRect.w)/2;
    logoRect.y = MARGIN;

    //Faire le rendu du logo
    SDL_RenderCopy(renderer, logo, NULL, &logoRect);
    
    //Afficher le menu
    SDL_Rect textRect = {
        logoRect.x,
        logoRect.y + logoRect.h + MARGIN,
        logoRect.w,
        44
    };
    renderTextCentered(bigGameFont, "Press (Enter) to start", textRect);
    textRect.y += 64;
    renderTextCentered(gameFont, "(LEFT, RIGHT) Move left or right", textRect);
    textRect.y += 44;
    renderTextCentered(gameFont, "(DOWN) Soft drop", textRect);
    textRect.y += 44;
    renderTextCentered(gameFont, "(UP) Hard drop", textRect);
    textRect.y += 44;
    renderTextCentered(gameFont, "(W, X) Rotate", textRect);
    textRect.y += 44;
    renderTextCentered(gameFont, "(Space) Put on hold", textRect);
    textRect.y += 64;
    renderTextCentered(gameFont, "(M) Music on/off", textRect);
    textRect.y += 44;
    renderTextCentered(gameFont, "(P) Pause", textRect);
    textRect.y += 44;
    renderTextCentered(gameFont, "(Esc) Quit", textRect);

    SDL_RenderPresent(renderer);
}

//Afficher l'écran de jeu
void renderGameScreen() {    
    SDL_RenderClear( renderer );
    renderBackground();
    renderGrid();
    renderHold();
    renderNext();
    renderCounters();
    renderShadowTetrimino();
    renderCurrentTetrimino();
    renderText(gameFont, "Hold", holdRect);
    renderText(gameFont, "Lines", linesRect);
    renderText(gameFont, "Level", levelRect);
    renderText(gameFont, "Score", scoreRect);
    renderText(gameFont, "Next", nextRect[0]);
    SDL_RenderPresent(renderer);
}

//Afficher Pause
void renderPausedScreen() {
    renderBig(bigFont, "PAUSE");
    SDL_RenderPresent(renderer);
}

//Afficher Game Over
void renderGameOverScreen() {
    renderBig(bigFont, "GAME OVER");
    SDL_RenderPresent(renderer);
}

//Afficher l'écran en fonction de l'état du jeu
void render(GAME_STATE state) {
    switch(state) {
        case startScreen:
            renderStartScreen();
            break;
        case playing:
            renderGameScreen();
            break;
        case paused:
            renderPausedScreen();
            break;
        case gameOver:
            renderGameOverScreen();
            break;
    }
}

//Charge une image sous forme de texture
SDL_Texture* loadTexture( const std::string &str ) {
	//Charge une image dans une surface 
	SDL_Surface* surface = IMG_Load( str.c_str() );

	//Convertit la surface (pixels bruts) en texture optimizée pour le matériel
	SDL_Texture* texture = SDL_CreateTextureFromSurface( renderer, surface );

	//Plus besoin de la surface, on libère la mémoire
	SDL_FreeSurface( surface );

	return texture;
}

//Extrait la texture d'un bloc de la sprite sheet
SDL_Texture* getBlockTexture(SDL_Surface* spriteSheet, SDL_Rect clip) {
    SDL_Surface* blockSurface = SDL_CreateRGBSurface(0, SQUARE_LENGTH, SQUARE_LENGTH, 32, 0, 0, 0, 0);
    SDL_Rect destination = { 0, 0, SQUARE_LENGTH, SQUARE_LENGTH };
    SDL_BlitSurface(spriteSheet, &clip, blockSurface, &destination);
    SDL_Texture* blockTexture = surfaceToTexture(blockSurface);
    return blockTexture;
}

//Convertit une SDL_Surface en SDL_Texture et libère la surface.
SDL_Texture* surfaceToTexture(SDL_Surface* surface) {
    SDL_Texture* text;
    text = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);
    return text;
}
