//
//  debug.h
//  tetris-clone
//
//  Fonctions pour afficher le contenu de certaines variables dans la console
//
//  Created by Arthur Pourrier on 02/05/2018.
//  Copyright © 2018 artnou. All rights reserved.
//

#ifndef debug_h
#define debug_h

#include "tetris_constants.h"

void printTetrimino(Tetrimino tetrimino);
void printMatrix(Uint8 matrix[23][16]);
void printScore();

extern Uint32 scoreCounter;
extern Uint32 linesCounter;
extern Uint32 levelCounter;

#endif /* debug_h */
