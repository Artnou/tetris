//
//  tetris_constants.h
//  tetris-clone
//
//  Enum, Structures et Constantes du jeu
//
//  Created by Arthur Pourrier on 20/04/2018.
//  Copyright © 2018 artnou. All rights reserved.
//

#ifndef tetris_constants_h
#define tetris_constants_h

#include <stdlib.h>
#include <SDL2/SDL.h>

enum GAME_STATE { startScreen, playing, paused, gameOver};
enum DIRECTION { left, right, top, down };
enum MUSIC { on, off };

//Definition d'un Tetrimino
typedef struct Tetrimino Tetrimino;
struct Tetrimino {
    SDL_Point position; // position de la case [0][0] dans la grille
    int rotation;       // rotation en cours (0 à 3);
    int model;          // modèle de tetrimino (1 à 7)
    Uint8 matrix[4][4]; // matrice 4x4 correspondant au modèle et la rotation en cours
};

//Dimensions
static const int SCREEN_WIDTH = 880;
static const int SCREEN_HEIGHT = 840;
static const int SIDE_LENGTH = 160;
static const int GRID_WIDTH = 400;
static const int GRID_HEIGTH = 800;
static const int MARGIN = 40;
static const int SQUARE_LENGTH = 40;

//Acceleration du jeu
static const int DELAY_START = 800;
static const int DELAY_OFFSET = 35;
static const int DELAY_MIN = 30;

static const SDL_Point START_POSITION = { 6, 19 };

static const struct Tetrimino EmptyTetrimino = { 0 }; // Pour remettre le tetrimino en cours à 0

//Tous les Tetriminos possibles, dans les 4 rotations
static const Uint8 TETRIMINOS[7][4][4][4] = {
    {                   // <- modèle 0 = barre
        {               // <- rotation 0
            {0,0,0,0},  // <- ligne 0, colonnes 0 à 3
            {1,1,1,1},  // <- ligne 1, colonnes 0 à 3
            {0,0,0,0},  // <- ligne 2, colonnes 0 à 3
            {0,0,0,0}   // <- ligne 3, colonnes 0 à 3
        },
        {               // <- rotation 1
            {0,0,1,0},  // <- ligne 0, colonnes 0 à 3
            {0,0,1,0},
            {0,0,1,0},
            {0,0,1,0}
        },
        {               // <- rotation 2
            {0,0,0,0},
            {1,1,1,1},
            {0,0,0,0},
            {0,0,0,0}
        },
        {               // <- rotation 3
            {0,0,1,0},
            {0,0,1,0},
            {0,0,1,0},
            {0,0,1,0}
        }
        
    },
    {                   // <- modèle 1 = T
        {               // <- rotation 0
            {0,0,0,0},
            {2,2,2,0},
            {0,2,0,0},
            {0,0,0,0}
        },
        {
            {0,0,0,0},
            {2,0,0,0},
            {2,2,0,0},
            {2,0,0,0}
        },
        {
            {0,0,0,0},
            {0,2,0,0},
            {2,2,2,0},
            {0,0,0,0}
        },
        {
            {0,0,0,0},
            {0,0,2,0},
            {0,2,2,0},
            {0,0,2,0}
        }
    },
    {
        {
            {0,0,0,0},
            {3,3,0,0},
            {0,3,3,0},
            {0,0,0,0}
        },
        {
            {0,0,0,0},
            {0,0,3,0},
            {0,3,3,0},
            {0,3,0,0}
        },
        {
            {0,0,0,0},
            {3,3,0,0},
            {0,3,3,0},
            {0,0,0,0}
        },
        {
            {0,0,0,0},
            {0,0,3,0},
            {0,3,3,0},
            {0,3,0,0}
        }
    },
    {
        {
            {0,0,0,0},
            {4,4,4,0},
            {4,0,0,0},
            {0,0,0,0}
        },
        {
            {0,0,0,0},
            {0,4,0,0},
            {0,4,0,0},
            {0,4,4,0}
        },
        {
            {0,0,0,0},
            {0,0,4,0},
            {4,4,4,0},
            {0,0,0,0}
        },
        {
            {0,0,0,0},
            {0,4,4,0},
            {0,0,4,0},
            {0,0,4,0}
        }
    },
    {
        {
            {0,0,0,0},
            {5,5,5,0},
            {0,0,5,0},
            {0,0,0,0}
        },
        {
            {0,0,0,0},
            {0,5,5,0},
            {0,5,0,0},
            {0,5,0,0}
        },
        {
            {0,0,0,0},
            {5,0,0,0},
            {5,5,5,0},
            {0,0,0,0}
        },
        {
            {0,0,0,0},
            {0,0,5,0},
            {0,0,5,0},
            {0,5,5,0}
        }
    },
    {
        {
            {0,0,0,0},
            {0,6,6,0},
            {6,6,0,0},
            {0,0,0,0}
        },
        {
            {0,0,0,0},
            {0,6,0,0},
            {0,6,6,0},
            {0,0,6,0}
        },
        {
            {0,0,0,0},
            {0,6,6,0},
            {6,6,0,0},
            {0,0,0,0}
        },
        {
            {0,0,0,0},
            {0,6,0,0},
            {0,6,6,0},
            {0,0,6,0}
        }
    },
    {
        {
            {0,0,0,0},
            {0,7,7,0},
            {0,7,7,0},
            {0,0,0,0}
        },
        {
            {0,0,0,0},
            {0,7,7,0},
            {0,7,7,0},
            {0,0,0,0}
        },
        {
            {0,0,0,0},
            {0,7,7,0},
            {0,7,7,0},
            {0,0,0,0}
        },
        {
            {0,0,0,0},
            {0,7,7,0},
            {0,7,7,0},
            {0,0,0,0}
        }
    }
};

#endif /* tetris_constants_h */
