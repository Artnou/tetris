//
//  debug.cpp
//  tetris-clone
//
//  Fonctions pour afficher le contenu de certaines variables dans la console
//
//  Created by Arthur Pourrier on 02/05/2018.
//  Copyright © 2018 artnou. All rights reserved.
//

#include "debug.h"

//DEBUG Imprimer les valeurs d'un Tetrimino dans la console
void printTetrimino(Tetrimino tetrimino) {
    printf("Position: x=%i, y=%i\n", tetrimino.position.x, tetrimino.position.y);
    printf("Rotation: %i\n", tetrimino.rotation);
    printf("Model: %i\n", tetrimino.model);
    printf("{\n");
    for (int i = 0; i < 4; i++) {
        printf("  { ");
        for (int j = 0; j < 4; j++) {
            printf("%i", tetrimino.matrix[i][j]);
            if (j < 3) { printf(", "); }
        }
        if (i < 4) {
            printf(" },\n");
        } else {
            printf(" }\n");
        }
    }
    printf("}\n");
}

//DEBUG Imprimer les valeurs d'une matrice
void printMatrix(Uint8 matrix[23][16]) {
    printf("{\n");
    for (int i = 0; i < 23; i++) {
        printf("  { ");
        for (int j = 0; j < 16; j++) {
            printf("%i", matrix[i][j]);
            if (j < 15) { printf(", "); }
        }
        if (i < 22) {
            printf(" },\n");
        } else {
            printf(" }\n");
        }
    }
    printf("}\n");
}

//DEBUG Imprimer le score
void printScore() {
    printf("Level: %i, lines: %i, score: %i\n", levelCounter, linesCounter, scoreCounter);
}
