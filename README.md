![Logo de Tetris](https://bitbucket.org/Artnou/tetris-clone/raw/c5ba421a2cbac0af523ef9f59e1544b4bd6061e3/SDL%20Tutorial/images/tetris-logo.png)

# Tetris SDL

Un tetris réalisé en C++ avec la librairie SDL

## Installation du programme

### Installer un environnement SDL qui fonctionne

Suivez les instructions site [SDL Officiel](http://wiki.libsdl.org/Installation) pour installer SDL.
Nous allons utiliser :
- SDL2
- SDL2_image pour les images
- SDL2_ttf pour le texte

#### Sur Mac
Le plus simple pour installer SDL2 est d'utiliser `brew`.

```
brew install sdl2 sdl2_image sdl2_ttf
```

### Gestion des bruitages et de la musique
Pour la gestion du son, nous utiliserons la bibliothèque audio [Simple-SDL2-Audio](https://github.com/jakebesworth/Simple-SDL2-Audio).
Nous intégrerons directement le code de Simple SDL2 Audio au projet donc il n'y a rien à installer pour le moment.

### GIT: Contrôle de code source
GIT est un outil génial pour travailler en équipe. Nous allons utiliser Bitbucket.org pour le projet.
Ca permettra de partager le code et de conserver un historique des modifications.

1. Installez GIT

Suivez les [instructions](https://www.atlassian.com/git/tutorials/install-git) pour installer Git sur votre machine

2. Créez votre compte bitbucket

Allez vous inscrire sur [Bitbucket](https://bitbucket.org/account/signup/) en choisissant un pseudo assez court qui ne soit pas utilisé

### Récupérez le projet Tetris-Clone

- Sur votre ordinateur, ouvrez un terminal
- Allez dans le dossier dans lequel vous voulez ajouter le projet.
- Tapez dans le terminal :
```bash
git clone git@bitbucket.org:Artnou/tetris-clone.git
```

Un nouveau dossier `tetris-clone` va être créé et contiendra le code du projet


## Documentation du projet

Demandez l'accès au document [Tetris Clone](https://docs.google.com/document/d/1hpkookKKpxv6_X5cg1-S1x7z1gyoOGWfAewmDO1l0OQ/edit#) sur Google Drive.

Ce document contient toutes les spécifications du projet.